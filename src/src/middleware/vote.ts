import { RootAction, RootState, Services } from 'SupplierTypes';
import * as SignalR from '@aspnet/signalr';
import { Observable, Observer, from, timer  } from 'rxjs';
import { retryWhen, timeInterval, delayWhen, map, catchError, tap  } from 'rxjs/operators';
import * as VoteActions from '../features/vote/actions';
import { ActionType } from 'typesafe-actions';
import * as VoteConstants from '../features/vote/constants';
import { normalize } from 'normalizr';
import { Guid } from 'guid-typescript';
let connection: SignalR.HubConnection;
export type VoteAction = ActionType<typeof VoteActions>;
export function voteInvokeMiddleware(store: any) {
    return (next: any) => async (action: VoteAction) => {
        switch (action.type) {
        case VoteConstants.CREATE_VOTE_CONNECTION: 
            voteRegisterCommands(store, action.payload.id, action.payload.url).subscribe(x =>{}, err => reconnect(store, action.payload.id, action.payload.url));
            break;   
        case VoteConstants.JOIN_EVENT:
            connection.invoke('Join', action.payload.id);
            break; 
        case VoteConstants.SEND_VOTE:
            connection.invoke('SendVote', action.payload.id, action.payload.feedback);
            break;
        }  
        return next(action);
    }
}

function reconnect(store: any, id: Guid, url: string){
    voteRegisterCommands(store, id, url)
    .pipe(retryWhen(errors => 
        errors.pipe(
            delayWhen(val => timer(1000)),
            tap(x => store.dispatch(VoteActions.reconnect(id))))
             
    ))
    .subscribe(x => console.info(`Reonnect success`));
}

function voteRegisterCommands(store: any, id: Guid, url: string): Observable<boolean>{
    return Observable.create((obs: Observer<boolean>) => {
        connection = new SignalR.HubConnectionBuilder()
        .withUrl(url)
        .build();
        connection.onclose(error=> {
            store.dispatch(VoteActions.disconnect(id));
            if(error){
                console.info(`Connection close with error ${error}`);
                reconnect(store, id, url);
             }
             else {
                 console.info("Connection stoped.");
             }
        });
        connection.on("receiveVote", (id, feedback) => {
            store.dispatch(VoteActions.receiveVote(id, feedback))
        });

        connection.on("addEvent", (id, name) => {
            store.dispatch(VoteActions.addEvent(id, name));
        });

        return from(connection.start())
        .pipe(
            map((x) => {
                store.dispatch(VoteActions.connect(id));
            }),
            map(x => true))
        .subscribe(obs);
    });
}