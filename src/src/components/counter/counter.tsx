import * as React from 'react';
import {Badge, BadgeProps } from 'react-bootstrap';
import './style.css';
type Props = {
  counter: number
} & BadgeProps

export const Counter: React.FC<Props> = props => {
    return (
      <Badge className="counter-badge" pill {...props}>{props.counter}</Badge>
    );
}
