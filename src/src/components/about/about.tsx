import  React, { useState, useEffect } from 'react';
import { Jumbotron } from 'react-bootstrap';
import axios from 'axios';
import './style.css';
type Props ={}

export const About: React.FC<Props> = props => {
  const [data, setData] = useState("");
  const URL = `${window.microshowApiUri}/api/values`;
  useEffect(() => {

      axios.get(URL).then(x =>{
           setData(x.data);
      });
  });
  return  (
      <div>
      <Jumbotron>
        <h1 className="display-3">Microshow</h1>	
        <p className="lead">App version: {process.env.REACT_APP_VERSION}</p>
        <p className="lead">URL: {URL}</p>
        <p className="lead">Test: {JSON.stringify(data)}</p>
        <hr className="my-2" />
        <p>App build time: {process.env.REACT_APP_BUILD_TIME}</p>
      </Jumbotron>
    </div>
    );
}
