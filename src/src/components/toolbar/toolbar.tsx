import * as React from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import './style.css';
import {faSmileWink, faSadTear, faGrinStars, faAngry} from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Counter } from "..";
import { Connection, CollectionState } from "../../models";
import { StatusIndicator } from "../../components";
import { RouteComponentProps } from 'react-router';
import {Guid} from 'guid-typescript';
import { LinkContainer } from 'react-router-bootstrap';
 
type Props = {
  connections: CollectionState<Guid, Connection>;
}
export const Toolbar: React.FC<Props> = props => {
  const { connections} = props;
  const renderStatusIndicators = () => {
    return (
      <Nav>
        {
          connections.allIds.map(id => {
            const connection = connections.byId.get(id);
            if(connection){
                return <StatusIndicator key={connection.id.toString()} connection={connection}/> 
              }
            }
          )
        }
     </Nav>);
  }
  return (
<Navbar bg="dark" variant="dark" collapseOnSelect expand="lg">
        <Navbar.Brand>
         <LinkContainer to="/">
           <Navbar.Brand>{"Microshow"}</Navbar.Brand>
         </LinkContainer> 
         </Navbar.Brand>
         <Navbar.Toggle aria-controls="main-navbar-nav" />
          <Navbar.Collapse key="main-navbar-nav">
            <Nav className="mr-auto">
            <LinkContainer to="/events">
              <Nav.Link eventKey={1} >Events</Nav.Link>
            </LinkContainer>

            <LinkContainer to="/about">
              <Nav.Link eventKey={3}>About</Nav.Link>
            </LinkContainer>
          </Nav>
          <Nav>
          { renderStatusIndicators() }
          </Nav>
        </Navbar.Collapse>
      </Navbar>
  );
}
