import * as React from 'react';
import * as classNames from 'classnames';
import { Connection } from '../../models';
import { Badge, Popover, OverlayTrigger, Button } from 'react-bootstrap'
import { Translate, I18n  } from 'react-redux-i18n';
import  './style.css';

type Props = {
  connection: Connection;
}

export const StatusIndicator: React.FC<Props> = props => {
  const {connection} = props;
  const popover = (
    <Popover id="popover-basic" title={connection.name}>
      <div>{connection.url}</div>
    </Popover>
  );
  var visualVariant: 'danger' | 'success' | 'warning' | 'dark' =  'dark';
  switch(connection.status){
    case 0: {
      visualVariant = 'danger';
      break;
    }
    case 1: {
      visualVariant = 'success';
      break;
    }
    case 2: {
      visualVariant = 'warning';
      break;
    }
  }

  return (
    <OverlayTrigger trigger="click" placement="bottom" overlay={popover}>
      <Badge pill variant={visualVariant}  className="status-indicator"> </Badge>
    </OverlayTrigger>
  );
}

