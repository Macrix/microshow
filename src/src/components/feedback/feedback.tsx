import * as React from 'react';
import { Jumbotron, Button, Col, Container, Row,Badge } from 'react-bootstrap';
import './style.css';
import {faSmileWink, faSadTear, faGrinStars, faAngry} from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Counter } from "../";
import { Connection } from "../../models";
import { RouteComponentProps } from 'react-router';
type Props = {
  awesomeCounter: number;
  goodCounter: number;
  sadCounter: number;
  angryCounter: number;
  eventId: string | undefined;
  eventName: string | undefined;
  connected: boolean;
  onSendVote: (string, number) => void
} & RouteComponentProps
export const Feedback: React.FC<Props> = props => {
  if(props.eventId === undefined){
    props.history.push("/");
  }
  const classname = "column-stretch " + (props.connected ? "connected": "disconnected");
  return (
      <div className={classname}>
      <h1 className="title">{props.eventName}</h1>
      <Container className="column-stretch">
        <Row className="feedback-row">  
          <Col className="feedback-col">
            <Button variant="light" onClick={() => props.onSendVote(props.eventId, 3)}><FontAwesomeIcon icon={faGrinStars} className="fa-8x" color="green"/></Button>
            <Counter variant="success" counter={props.awesomeCounter}/>
          </Col>
          <Col className="feedback-col">
            <Button variant="light" onClick={() => props.onSendVote(props.eventId, 2)}><FontAwesomeIcon icon={faSmileWink} className="fa-8x"color="green"/></Button>
            <Counter variant="success" counter={props.goodCounter}/>
          </Col>
        </Row>
        <Row className="feedback-row">
          <Col className="feedback-col">
            <Button variant="light" onClick={() => props.onSendVote(props.eventId, 1)}><FontAwesomeIcon icon={faSadTear} className="fa-8x" color="red"/></Button>
            <Counter variant="danger" counter={props.sadCounter}/> 
          </Col>
          <Col className="feedback-col">
            <Button variant="light" onClick={() => props.onSendVote(props.eventId, 0)}><FontAwesomeIcon icon={faAngry} className="fa-8x" color="red"/></Button>
            <Counter variant="danger" counter={props.angryCounter}/>   
          </Col>
        </Row>
      </Container>
    </div>
  );
}
