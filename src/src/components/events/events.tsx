import * as React from 'react';
import { Jumbotron, Button, Col, Container, Row, ListGroup } from 'react-bootstrap';
import './style.css';
import {faSmileWink, faSadTear, faGrinStars, faAngry} from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Counter } from "..";
import { Connection, CollectionState } from "../../models";
import {  RouteComponentProps } from 'react-router';
type Props = {
  events: CollectionState<string, string>;
  connected: boolean;
  onJoinEvent: (number) => void
} & RouteComponentProps
export const Events: React.FC<Props> = props => {
  const classname = "column-stretch " + (props.connected ? "connected": "disconnected");
  return (
    <div className={classname}>
      <h1 className="title">Events</h1>
      <Container className="column-stretch">
        <ListGroup>
        {
          props.events.allIds.map(id =>{
            return (          
            <ListGroup.Item key={id} action variant="success" onClick={() =>{
              props.history.push(`/feedback/${id}`);
              props.onJoinEvent(id);
            }}>
            {props.events.byId.get(id)}
          </ListGroup.Item>);
          })
        }
        </ListGroup>
      </Container>
    </div>
  );
}
