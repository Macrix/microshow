import * as React from 'react';
import Types from 'SupplierTypes';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { Toolbar } from '..';
import * as action from '../../features/vote/actions';
import { getAngryCounter, getAwesomeCounter, getGoodCounter, getSadCounter, getEventName, getEventId } from '../../features/vote/selector'
import { withRouter, RouteComponentProps } from 'react-router';
import { eventNames } from 'cluster';

const mapStateToProps = (state: Types.RootState, props) =>  ({
  connections: state.vote.connections,
});

const mapDispatchToProps = (dispatch: Dispatch<Types.RootAction>) =>
  bindActionCreators(
    {
      onSendVote: action.sendVote
    },
    dispatch
);

export const ToolbarConnected 
= withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Toolbar));

