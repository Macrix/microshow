import * as React from 'react';
import Types from 'SupplierTypes';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { Feedback } from '../';
import * as action from '../../features/vote/actions';
import { getAngryCounter, getAwesomeCounter, getGoodCounter, getSadCounter, getEventName, getEventId, getConnection } from '../../features/vote/selector'
import { withRouter, RouteComponentProps } from 'react-router';
import { eventNames } from 'cluster';
type OwnProps = RouteComponentProps<{eventId: string}>;

const mapStateToProps = (state: Types.RootState, props: OwnProps) =>  ({
  awesomeCounter: getAwesomeCounter(state.vote),
  goodCounter: getGoodCounter(state.vote),
  sadCounter:  getSadCounter(state.vote),
  angryCounter:  getAngryCounter(state.vote),
  eventId: getEventId(state.vote, props.match.params.eventId),
  eventName: getEventName(state.vote, props.match.params.eventId),
  connected: getConnection(state.vote)
});

const mapDispatchToProps = (dispatch: Dispatch<Types.RootAction>) =>
  bindActionCreators(
    {
      onSendVote: action.sendVote
    },
    dispatch
);

export const FeedbackConnected 
= withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Feedback));

