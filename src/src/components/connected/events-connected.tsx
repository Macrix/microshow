import * as React from 'react';
import Types from 'SupplierTypes';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { Events } from '..';
import * as action from '../../features/vote/actions';
import { getAngryCounter, getAwesomeCounter, getGoodCounter, getSadCounter, getConnection } from '../../features/vote/selector'
import { withRouter, RouteComponentProps } from 'react-router';
const mapStateToProps = (state: Types.RootState, props) =>  ({
  events: state.vote.events,
  connected: getConnection(state.vote)
});

const mapDispatchToProps = (dispatch: Dispatch<Types.RootAction>) =>
  bindActionCreators(
    {
      onJoinEvent: action.joinEvent
    },
    dispatch
);

export const EventsConnected 
= withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Events));

