declare global {
    interface Window { microshowApiUri?: string; }
}

window.microshowApiUri = window.microshowApiUri || "https://microshowgateway.herokuapp.com";

export { };	