import { AjaxError } from "rxjs/ajax";
import {Guid} from 'guid-typescript';
export enum ConnectionType{
  OneWay = 0, Duplex = 1
}


export class CollectionState<TKey, TValue> {
  byId: Map<TKey, TValue>;
  allIds: Array<TKey>;
  currentPage: number;
  resultsPerPage: number;
  totalPages: number;
  totalResults: number;
  isLoading: boolean;
  error: Error | undefined;
  constructor() {
    this.byId = new Map<TKey, TValue>();
    this.allIds = new Array<TKey>();
  }
}

export class Connection{
  constructor(id: Guid, url: string, name: string, type: ConnectionType){
    this.id = id;
    this.url = url;
    this.name = name;
    this.type = type;
  }
  readonly id: Guid; 
  readonly url: string;
  readonly name: string;
  readonly type: ConnectionType;
  status: number = 0;
  reconnectCounter: number = 0;
}
