

export const resolveWithDelay = <T>(value: T, time: number = 1000) => new Promise(
  (resolve) => setTimeout(() => resolve(value), time)
);

export const rangeQueryString = (pageRequest?: any) =>
pageRequest != undefined ? `&Page=${pageRequest.pagenNumber}&Results=${pageRequest.pageSize}` : "";
  

export const removeKeys = <T>(payload: T, keys: Array<keyof T>) => {
  keys.forEach((key) => {
    delete payload[key];
  });

  return payload;
};
