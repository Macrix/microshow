import {agent} from '.';

const URL = `${window.microshowApiUri}/api/v1/operations/`;
export const OperationsApi = {
  get: (request: any) => agent.get<any>(`${URL}/${request.id}`)
};
