import * as logger from './logger-service';
import * as localStorage from './local-storage-service';
import { OperationsApi, agent } from '../api';
export default {
  logger,
  localStorage,
  OperationsApi,
  agent
};
