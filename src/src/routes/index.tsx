import React from 'react'
import { Route, Switch, Redirect } from 'react-router'
import {NotFound, About, FeedbackConnected, EventsConnected } from '../components'
const routes = (
  <Switch>
    <Redirect exact from="/" to="events"/> 
    <Route path="/events" component={EventsConnected} />
    <Route path="/feedback/:eventId"  component={FeedbackConnected} />
    <Route path="/about" component={About} />
    <Route path="*" component={NotFound} />
  </Switch>
  )

export default routes