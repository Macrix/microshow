import React from 'react'
import {History} from 'history'
import { ConnectedRouter } from 'connected-react-router'
import routes from './routes'
import { ToolbarConnected } from './components';
interface AppProps {
  history: History;
}

const App = ({ history }: AppProps) => {
  return ( 
     <ConnectedRouter history={history}>
       <ToolbarConnected/>
      <div className="body">
        { routes }
      </div>
    </ConnectedRouter>
  )
}

export default App