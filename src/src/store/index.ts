import { RootAction, RootState, Services } from 'SupplierTypes';
import { createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { createBrowserHistory } from 'history'
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './root-reducer';
import rootEpic from './root-epic';
import { voteInvokeMiddleware} from '../middleware';
import rootAction from './root-action';
import services from '../services';
import { syncTranslationWithStore, setLocale, loadTranslations } from 'react-redux-i18n';
import thunk from 'redux-thunk';
import de from '../translations/de-DE.json';
import en from '../translations/en-US.json';
import pl from '../translations/pl-PL.json';

export const epicMiddleware = createEpicMiddleware<
  RootAction,
  RootAction,
  RootState,
  Services
>({
  dependencies: services,
});

// configure middlewares
const middlewares = [epicMiddleware];
// compose enhancers
const enhancer = composeWithDevTools(applyMiddleware( voteInvokeMiddleware, ...middlewares, thunk));

// rehydrate state on app start
const initialState = {};
 
// create store
const store = createStore(rootReducer, initialState, enhancer);

epicMiddleware.run(rootEpic);
syncTranslationWithStore(store)
store.dispatch(rootAction.auth.checkAuthentication());


export default store;
