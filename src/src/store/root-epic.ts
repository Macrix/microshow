import { combineEpics } from 'redux-observable';

import * as authEpics from '../features/auth/epics';
import * as voteEpics from '../features/vote/epics';
export default combineEpics(
    ...Object.values(authEpics),
    //...Object.values(voteEpics),
    );
