declare module 'SupplierTypes' {
  import { DispatchCallback, setLocale } from 'react-redux-i18n';
  import { StateType, ActionType } from 'typesafe-actions';
  export type Store = StateType<typeof import('./index').default>;
  export type RootAction = ActionType<typeof import('./root-action').default>;
  export type RootState = StateType<typeof import('./root-reducer').default>;
}
declare module '*.css' {
  const styles: any;
  export = styles;
}

declare module "*.json" {
  const value: any;
  export default value;
}