
import { routerReducer } from 'react-router-redux';
import { combineReducers, AnyAction } from 'redux';
import { History } from 'history'
import { createBrowserHistory } from 'history'
import { connectRouter } from 'connected-react-router'
import voteReducer from '../features/vote/reducer';

export const history = createBrowserHistory()
const rootReducer =  combineReducers({
  vote: voteReducer,
  router: connectRouter(history),
});

export default rootReducer;
