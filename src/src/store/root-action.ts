import { routerActions } from 'react-router-redux';

import * as authActions from '../features/auth/actions';
import * as voteActions from '../features/vote/actions';
import  { setLocale, loadTranslations } from 'react-redux-i18n';
export default {
  router: routerActions,
  auth: authActions,
  vote: voteActions,
  translation: {setLocale, loadTranslations}
};
