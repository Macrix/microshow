import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import store from './store';
import {history} from './store/root-reducer';
import App from './App'
import * as global from './models/windowExt';
import 'rxjs';
import 'bootstrap/dist/css/bootstrap.css';
import './themes/dark.css';
const Root = (
  <Provider store={store}>
     <App history={history} />
  </Provider>
);
ReactDOM.render(Root, document.getElementById('root'));

serviceWorker.register();
