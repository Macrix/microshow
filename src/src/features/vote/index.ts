// public API
import * as voteConstants from './constants';
import * as voteActions from './actions';
import voteReducer, { VoteState, VoteAction } from './reducer';
export {
  voteConstants,
  voteActions,
  voteReducer
};
export type VoteStateAlias = VoteState;
export type VoteActionAlias = VoteAction;