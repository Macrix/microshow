import { createSelector } from 'reselect';
import { VoteStateAlias } from './';

export const getAwesomeCounter = (state: VoteStateAlias) => {
   const vote = state.votes.byId.get(3);
   return vote || 0;
};
export const getGoodCounter = (state: VoteStateAlias) => {
    const vote = state.votes.byId.get(2);
    return vote || 0; 
} 
export const getSadCounter = (state: VoteStateAlias) => {
    const vote = state.votes.byId.get(1);
    return vote || 0; 
}
export const getAngryCounter = (state: VoteStateAlias) => {
    const vote = state.votes.byId.get(0);
    return vote || 0;
}
export const getEventName = (state: VoteStateAlias, id: string) : string | undefined => {
    return state.events.byId.get(id);
}
export const getEventId = (state: VoteStateAlias, id: string) : string | undefined => {
    var event =  state.events.byId.get(id);
    return event !== undefined ? id : undefined; 
}
export const getConnection = (state: VoteStateAlias) : boolean => {
    var connection = state.connections.byId.get(state.connections.allIds[0]);
    if(connection){
        return connection.status === 1;
    }
    return false; 
}