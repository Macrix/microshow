export const JOIN_EVENT = 'events/JOIN_EVENT';
export const ADD_EVENT = 'events/ADD_EVENT';
export const CREATE_VOTE_CONNECTION = 'vote/CREATE_VOTE_CONNECTION';
export const RECEIVE_VOTE = 'vote/RECEIVE_VOTE';
export const SEND_VOTE = 'vote/SEND_VOTE';
export const DISCONNECT = 'vote/DISCONNECT';
export const RECONNECT = 'vote/RECONNECT';
export const CONNECT = 'vote/CONNECT';
