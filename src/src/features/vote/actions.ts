import cuid from 'cuid';
import { action } from 'typesafe-actions';
import { ConnectionType } from '../../models';
import { Guid } from 'guid-typescript';
import { RECONNECT, CONNECT, DISCONNECT, CREATE_VOTE_CONNECTION, SEND_VOTE, RECEIVE_VOTE, JOIN_EVENT, ADD_EVENT } from './constants';

export const createVoteConnection = (id: Guid, url: string, name: string, type: ConnectionType) => action(CREATE_VOTE_CONNECTION, {id, url, name, type});
export const joinEvent = (id: string) => action(JOIN_EVENT, {id});
export const addEvent = (id: string, name: string) => action(ADD_EVENT, {id, name});
export const sendVote = (id: string, feedback: number) => action(SEND_VOTE, {id, feedback});
export const receiveVote = (id: string, feedback: number) => action(RECEIVE_VOTE, {id, feedback});
export const disconnect = (id: Guid) => action(DISCONNECT, {id});
export const reconnect = (id: Guid) => action(RECONNECT, {id});
export const connect = (id: Guid) => action(CONNECT, {id});