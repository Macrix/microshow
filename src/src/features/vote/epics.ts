import { RootAction, RootState, Services } from 'SupplierTypes';
import { Epic } from 'redux-observable';
import { tap, ignoreElements, filter, mergeMap, map, switchMap, catchError } from 'rxjs/operators';
import { isOfType, isActionOf } from 'typesafe-actions';
import { voteConstants, voteActions, VoteActionAlias } from '.';
import { ajax, AjaxError } from 'rxjs/ajax';
import { Observable, of, from, pipe } from 'rxjs';
import { AxiosError } from 'axios';

// export const createOperationFlow: Epic<RootAction, RootAction, RootState, Services> = (
//   action$,
//   state$,
//   { OperationsApi }
// ) =>
//   action$.pipe(
//     filter(isActionOf(voteActions.createVoteConnection)), 
//     switchMap(action =>
//       from(OperationsApi.get(action.payload)).pipe(
//          map(x => operationsActions.getOperation.success(x.data)),
//          catchError(x => of(operationsActions.getOperation.failure(x)))
//       )
//   )
// );
