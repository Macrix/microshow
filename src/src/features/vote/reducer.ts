import { combineReducers } from 'redux';
import { ActionType } from 'typesafe-actions';
import { Connection, CollectionState, ConnectionType} from '../../models';
import * as actions from './actions';
import { CONNECT, CREATE_VOTE_CONNECTION, DISCONNECT, RECONNECT, RECEIVE_VOTE, SEND_VOTE, ADD_EVENT, JOIN_EVENT } from './constants';
import update from 'immutability-helper';
import { Guid } from "guid-typescript";

export type VoteState = {
  connections: CollectionState<Guid, Connection>;
  events: CollectionState<string, string>;
  votes: CollectionState<number, number>;
};

export type VoteAction = ActionType<typeof actions>;

export default combineReducers<VoteState, VoteAction>({
  connections: (state = new CollectionState<Guid, Connection>(), action) => {
    switch (action.type) {
        case CREATE_VOTE_CONNECTION: {
          const connection = new Connection(action.payload.id, action.payload.url, action.payload.name, ConnectionType.Duplex);
          const copiedState = {...state};
          copiedState.byId = update(state.byId, {$add: [[connection.id, connection]]});
          copiedState.allIds = update(state.allIds, {$push: [connection.id]});  
          return copiedState; 
        }
        case DISCONNECT: {
          const copiedState = {...state};
          const connection = copiedState.byId.get(action.payload.id);
          if(connection)
            copiedState.byId.set(connection.id, update(connection, {status: {$set: 0} }));
          return copiedState;  
        }
        case CONNECT: {
          const copiedState = {...state};
          const connection = copiedState.byId.get(action.payload.id);
          if(connection)
            copiedState.byId.set(connection.id, update(connection, {status: {$set: 1},reconnectCounter:{$set: 0} }));
          return copiedState;
        }
        case RECONNECT: {
          const copiedState = {...state};
          const connection = copiedState.byId.get(action.payload.id);
          if(connection)
            copiedState.byId.set(connection.id, update(connection, {status: {$set: 2},reconnectCounter:{$apply: (x: number) => x + 1} }));
          return copiedState;  
        }
      default:
        return state;
    }
  },
  votes: (state = new CollectionState<number, number>(), action) => {
    switch (action.type) {
        case RECEIVE_VOTE: { 
          const copiedState = {...state};
          const vote = copiedState.byId.get(action.payload.feedback);
          if(vote !== undefined){
            copiedState.byId.set(action.payload.feedback, vote + 1 );
          }
          else{
            copiedState.byId = update(state.byId, {$add: [[action.payload.feedback, 1]]});
            copiedState.allIds = update(state.allIds, {$push: [action.payload.feedback]});  
          }
          return copiedState; 
        }
        case JOIN_EVENT: { 
          return new CollectionState<number, number>(); 
        }
      default:
        return state;
    }
  },
  events: (state = new CollectionState<string, string>(), action) => {
    switch (action.type) {
        case ADD_EVENT: { 
          const copiedState = {...state};
          const vote = copiedState.byId.get(action.payload.id);
          if(vote !== undefined){
            copiedState.byId.set(action.payload.id, action.payload.name );
          }
          else{
            copiedState.byId = update(state.byId, {$add: [[action.payload.id, action.payload.name]]});
            copiedState.allIds = update(state.allIds, {$push: [action.payload.id]});  
          }
          return copiedState; 
        }
      default:
        return state;
    }
  },
});
