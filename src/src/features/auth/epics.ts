import { RootAction, RootState, Services } from 'SupplierTypes';
import { Epic } from 'redux-observable';
import { tap, ignoreElements, filter, mergeMap, map, switchMap, catchError } from 'rxjs/operators';
import { isOfType, isActionOf } from 'typesafe-actions';
import { authConstants, authActions } from '.';
import { voteActions } from '../vote';
import { ajax, AjaxError } from 'rxjs/ajax';
import { Observable, of, from, pipe, empty, concat, } from 'rxjs';
import { flatMap} from 'rxjs/operators';
import { ConnectionType } from '../../models';
import { Guid } from 'guid-typescript';
      
export const checkAuthFlow: Epic<RootAction, RootAction, RootState, Services> = (
  action$,
  state$,
  {  }
) =>
  action$.pipe(
    filter(isOfType(authConstants.CHECK_AUTHENTICATION_REQUEST)), 
    switchMap(action => {
      if(sessionStorage.getItem('auth_token')){
          return of(authActions.loginUser.success(""))
        }  else {
          return of(authActions.loginUser.success(""))
        }
      })
  );

  export const authFlow: Epic<RootAction, RootAction, RootState, Services> = (
    action$,
    state$,
    {  }
  ) =>
    action$.pipe(
      filter(isOfType(authConstants.LOGIN_USER_SUCCESS)), 
      switchMap(action => 
        concat(
          of(voteActions.createVoteConnection(Guid.create(), window.microshowApiUri + `/events`, 'events', ConnectionType.Duplex))
        )
    )
  );