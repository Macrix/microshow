// public API
import * as authConstants from './constants';
import * as authActions from './actions';
// import productsReducer, { ProductsState, ProductsAction } from './reducer';
export {
  authConstants,
  authActions,
  //authReducer
};
// export type AuthStateAlias = AuthState;
// export type AuthActionAlias = AuthAction;