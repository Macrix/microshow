import cuid from 'cuid';
import { action,createStandardAction,createAsyncAction} from 'typesafe-actions';
import { CHECK_AUTHENTICATION_REQUEST, LOGIN_USER_REQUEST, LOGIN_USER_SUCCESS, LOGIN_USER_FAILURE} from './constants';


export const checkAuthentication = () => action(CHECK_AUTHENTICATION_REQUEST);

export const loginUser = createAsyncAction(
  LOGIN_USER_REQUEST,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILURE
)<any, any, Error>();

