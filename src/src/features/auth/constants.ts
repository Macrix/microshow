export const CHECK_AUTHENTICATION_REQUEST = 'auth/CHECK_AUTHENTICATION_REQUEST';
export const LOGIN_USER_REQUEST = 'auth/LOGIN_USER_REQUEST';
export const LOGIN_USER_FAILURE = 'auth/LOGIN_USER_FAILURE';
export const LOGIN_USER_SUCCESS = 'auth/LOGIN_USER_SUCCESS';