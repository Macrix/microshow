#!/bin/bash
echo "Starting application..."
if [[ -z ${API_BASE_URL+x} ]]; then
	export API_BASE_URL=http://localhost:5000
fi
echo "API_BASE_URL = ${API_BASE_URL}"
envsubst < "/usr/share/nginx/html/env-config.template.js" > "/usr/share/nginx/html/env-config.js"
envsubst '${PORT}' < "/etc/nginx/conf.d/default.conf.template" > "/etc/nginx/conf.d/default.conf"
nginx -g 'daemon off;'